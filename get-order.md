# Получение статуса заказа

## Пример HTTP запроса:

~~~~
GET http://evos-order.webtaxi.mobi:7654/evos/v2.0/order?idOrder=e51-1234 HTTP/1.1
Token: ${token}
Owner: ${owner}
Id-Partner: ${id}
Api-Key: ${key}
~~~~

## Описание параметров:

- **idOrder** - идентификатор заказа.

## Возможные варианты ответов:

- Статус **200** - Запрос успешен. Модель ответа:

~~~~
{
  "idOrder" : "p31-234",
  "status" : "created",
  "updated" : "2018-04-11T10:08:11Z",
  "orderRoute" : {
    "departure" : {
      "n" : "бул. Тараса Шевченко, 32",
      "c" : "бул. Тараса Шевченко, 32",
      "p" : [ 30.50450, 50.44530 ]
    },
    "destination" : [ {
      "n" : "бул. Тараса Шевченко, 32",
      "c" : "бул. Тараса Шевченко, 32",
      "p" : [ 30.50450, 50.44530 ]
    } ]
  },
  "driver" : {
    "id" : 12,
    "name" : "Dimon",
    "pushToken" : "5a9d2757c109b9d530aa60",
    "phones" : [ "" ],
    "vibers" : [ "" ],
    "car" : {
      "category" : "taxi",
      "class" : "economical",
      "carCode" : "skoda--octavia",
      "carName" : "Custom name",
      "color" : "beige",
      "number" : "AA2017BT",
      "photo" : "c9db0312b58443eda1ecbc3f2c1720b5.jpg"
    },
    "wifi" : true,
    "card" : true,
    "insured" : true,
    "documentLinks" : [ "" ]
  },
  "cost" : 282,
  "distance" : 36.148,
  "costDetails" : {
    "minCost" : 30,
    "kmMinCost" : 2,
    "freeWait" : 600,
    "inCity" : {
      "distance" : 3.773,
      "cost" : 39.40
    },
    "outCity" : {
      "distance" : 3.773,
      "cost" : 39.40
    },
    "rate" : [ {
      "type" : "weekend",
      "priceChange" : {
        "vector" : "surcharge",
        "value" : 10,
        "type" : "fixed"
      },
      "cost" : 24.56
    } ],
    "services" : [ {
      "code" : "conditioner",
      "priceChange" : {
        "vector" : "surcharge",
        "value" : 10,
        "type" : "fixed"
      }
    }, 12.89 ],
    "extra" : {
      "vector" : "surcharge",
      "value" : 10,
      "type" : "fixed",
      "cost" : 10
    },
    "total" : 34.67,
    "custom" : 34.67
  },
  "client" : {
    "name" : "Дмитрий",
    "phones" : [ "380502621249" ],
    "vibers" : [ "380502621249" ]
  },
  "orderTime" : "2018-04-11T10:08:11Z"
}
~~~~

В теле ответа возвращается данные заказа:

Возможные статусы:

- **created** - заказ создан,
- **accepted** - заказ был принят водителем,
- **left** - водитель выехал к клиенту,
- **waiting** - водитель ожидает клиента,
- **replied** - клиент ответил водителю о том что выходит,
- **went** - водитель выехал с клиентом к пункту назначения,
- **completed** - заказ выполнен,
- **canceled** - заказ отменен,
- **refused** - водитель отменил заказа,
- **refuse_confirmed** - оператор подтвердил отмену заказа водителем

- Статус **403** - Если idPartner не является владельцем заказа с идентификатором idOrder
- Статус **404** - Заказ не существует
- Статус **418** - Если заказ не был создан evos



